function getAngkaTerbesarKedua(getNumbers) {
    if (getNumbers == null) {
        //return "salah";
        throw "Parameter tidak boleh kosong";
    } else if (typeof getNumbers != "object") {
        throw "Data Tidak valid, harus berupa array";
    }

    // 1. menghilangkan angka yang kembar
    const fixNumber = [...new Set(getNumbers)];

    // 2. menyortir angka terkecil ke terbesar
    fixNumber.sort(function (a, b) {
        return a - b;
    });

    // 3. menghapus index terakhir
    fixNumber.pop();

    // 4. return angka tertinggi
    return Math.max(...fixNumber);
}

const dataAngka = [9, 4, 7, 7, 4, 3, 2, 2, 8];
try {
    console.log(getSplitName(dataAngka)); //True
    console.log(getSplitName(0)); //Error
    console.log(getSplitName()); //Error
} catch (error) {
    console.log("Error : " + error);
}
