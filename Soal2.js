const checkTypeNumber = (givenNumber) => {
    if (givenNumber == null) {
        //return "salah";
        throw "Bro where is the parameter?";
    } else if (typeof givenNumber != "number") {
        throw "Invalid data type";
    }

    if (givenNumber % 2 == 0) {
        return "GENAP";
    } else {
        return "GANJIL";
    }
};

try {
    console.log(checkTypeNumber(10));
    console.log(checkTypeNumber(3));
    console.log(checkTypeNumber("3"));
    //console.log(checkTypeNumber({}));
    //console.log(checkTypeNumber([]));
    //console.log(checkTypeNumber());
} catch (error) {
    console.log("Error : " + error);
}
